# QCollect (C Collections library)

## Introduction
QCollect is a C-11 library which aims to contain implementations of different collection types. This is a personal project at the moment not intended for distribution.

## Currently implemented features
* Linked list
* Array list

## License
QCollect is free software and licensed under the **GPLv2** license. You can find a copy of the license in the LICENSE file provided in the source code or by browsing the [GNU website](http://www.gnu.org/licenses).

## Bugs and suggestions
If you're willing to submit bug reports or suggestions for the project, you can use the [Bug Tracker](https://github.com/alkafir/qcollect/issues) or directly [email me](mailto:alfredo.mungo@openmailbox.org).
