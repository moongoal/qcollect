#include <alist.h>
#include "qunit.h"

QUNIT_TEST(create_delete) {
  QC_AL *alist = qc_al_init();

  qunit_assert(alist != NULL);

  qc_al_free(alist);
}

QUNIT_TEST(add) {
  QC_AL *alist = qc_al_init();
  QC_AL_NODE *it, *node1, *node3;
  size_t i;

  QC_VALUE one, two, three;
  one.i = 1;
  two.i = 2;
  three.i = 3;

  node1 = qc_al_append(alist, one);
  qc_al_append(alist, two);
  node3 = qc_al_append(alist, three);

  for(i = 0; i < alist->length; i++) {
    it = alist->arr[i];
    qunit_assert(it->val.i > 0 && it->val.i < 4);
  }

  qunit_assert(alist->arr[0] == node1);
  qunit_assert(alist->arr[alist->length - 1] == node3);

  qc_al_free(alist);
}

QUNIT_TEST(remove) {
  QC_AL *alist = qc_al_init();
  QC_AL_NODE *node1, *node2, *node3;

  QC_VALUE one, two, three;
  one.i = 1;
  two.i = 2;
  three.i = 3;

  node1 = qc_al_append(alist, one);
  node2 = qc_al_append(alist, two);
  node3 = qc_al_append(alist, three);

  qc_al_remove(alist, node1);
  qc_al_remove(alist, node2);
  qc_al_remove(alist, node3);

  qunit_assert(alist->length == 0);

  qc_al_free(alist);
}

QUNIT_TEST(insert) {
  QC_AL *alist = qc_al_init();
  QC_AL_NODE *node2;
  QC_VALUE one, two, three;
  one.i = 1;
  two.i = 2;
  three.i = 3;

  qc_al_append(alist, one);
  node2 = qc_al_append(alist, two);
  qc_al_insert_before(alist, three, node2);

  qunit_assert(alist->arr[0]->val.i == 1);
  qunit_assert(alist->arr[1]->val.i == 3);
  qunit_assert(alist->arr[2]->val.i == 2);

  qc_al_free(alist);
}

QUNIT_TEST(capacity) {
  QC_AL *alist = qc_al_init2(1);

  QC_VALUE val = { .i = 1 };

  qc_al_append(alist, val);
  qc_al_append(alist, val);

  qunit_assert(alist->length == 2);

  qc_al_free(alist);
}

int main() {
  QUNIT_TESTCASE tcase;

  qunit_tcase_init(&tcase);

  qunit_tcase_add(&tcase, create_delete);
  qunit_tcase_add(&tcase, add);
  qunit_tcase_add(&tcase, remove);
  qunit_tcase_add(&tcase, insert);
  qunit_tcase_add(&tcase, capacity);
  
  qunit_print_header();
  unsigned int failed = qunit_tcase_run(&tcase);

  printf("Failed tests: %u.\n", failed);

  return (int)failed;
}
