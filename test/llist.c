#include <llist.h>
#include "qunit.h"

QUNIT_TEST(create_delete) {
  QC_LL *llist = qc_ll_init();

  qunit_assert(llist != NULL);

  qc_ll_free(llist);
}

QUNIT_TEST(add) {
  QC_LL *llist = qc_ll_init();
  QC_LL_NODE *it, *node1, *node3;

  QC_VALUE one, two, three;
  one.i = 1;
  two.i = 2;
  three.i = 3;

  node1 = qc_ll_append(llist, one);
  qc_ll_append(llist, two);
  node3 = qc_ll_append(llist, three);

  for(it = llist->first; it != NULL; it = it->next) {
    qunit_assert(it->val.i > 0 && it->val.i < 4);
  }

  qunit_assert(llist->first == node1);
  qunit_assert(llist->last == node3);

  qc_ll_free(llist);
}

QUNIT_TEST(remove) {
  QC_LL *llist = qc_ll_init();
  QC_LL_NODE *node1, *node2, *node3;

  QC_VALUE one, two, three;
  one.i = 1;
  two.i = 2;
  three.i = 3;

  node1 = qc_ll_append(llist, one);
  node2 = qc_ll_append(llist, two);
  node3 = qc_ll_append(llist, three);

  qc_ll_remove(llist, node1);
  qc_ll_remove(llist, node2);
  qc_ll_remove(llist, node3);

  qunit_assert(llist->first == NULL);
  qunit_assert(llist->last == NULL);

  qc_ll_free(llist);
}

QUNIT_TEST(insert) {
  QC_LL *llist = qc_ll_init();
  QC_LL_NODE *node2;
  QC_VALUE one, two, three;
  one.i = 1;
  two.i = 2;
  three.i = 3;

  qc_ll_append(llist, one);
  node2 = qc_ll_append(llist, two);
  qc_ll_insert_before(llist, three, node2);

  qunit_assert(llist->first->val.i == 1);
  qunit_assert(llist->first->next->val.i == 3);
  qunit_assert(llist->first->next->next->val.i == 2);

  qunit_assert(llist->last->val.i == 2);
  qunit_assert(llist->last->prev->val.i == 3);
  qunit_assert(llist->last->prev->prev->val.i == 1);

  qc_ll_free(llist);
}

int main() {
  QUNIT_TESTCASE tcase;

  qunit_tcase_init(&tcase);

  qunit_tcase_add(&tcase, create_delete);
  qunit_tcase_add(&tcase, add);
  qunit_tcase_add(&tcase, remove);
  qunit_tcase_add(&tcase, insert);
  
  qunit_print_header();
  unsigned int failed = qunit_tcase_run(&tcase);

  printf("Failed tests: %u.\n", failed);

  return (int)failed;
}
