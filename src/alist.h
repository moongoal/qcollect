/*
 *    Qcollect: C collections library
 *    Copyright (C) 2015  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifndef _QCOLLECT_ALIST_H
#define _QCOLLECT_ALIST_H

#include <stddef.h>
#include "common.h"

/* Default Array List capacity */
#define QCOLLECT_AL_DEFAULT_CAPACITY 16

/* Default Array List capacity increase step */
#define QCOLLECT_AL_DEFAULT_CAPACITY_INCREASE 8

struct QCOLLECT_ALIST_NODE {
  union QCOLLECT_VALUE val;
};

struct QCOLLECT_ALIST {
  struct QCOLLECT_ALIST_NODE **arr;
  size_t capacity, length;
};

typedef struct QCOLLECT_ALIST_NODE QC_AL_NODE;
typedef struct QCOLLECT_ALIST QC_AL;

/*
 * Initializes a new array list instance with default capacity.
 * The returned value should be freed with qc_al_free().
 *
 * Return value:
 *  A new array list object pointer
 */
QC_AL *qc_al_init();

/*
 * Initializes a new array list instance with the specified capacity.
 * The returned value should be freed with qc_al_free().
 *
 * Arguments:
 *  init_cap: The initial list capacity
 *
 * Return value:
 *  A new array list object pointer
 */
QC_AL* qc_al_init2(size_t init_cap);

/*
 * Frees any memory associated with a array list instance.
 *
 * Arguments:
 *  al: A pointer to the array list object to free
 */
void qc_al_free(QC_AL *al);

/*
 * Inserts a node after another.
 *
 * Arguments:
 *  al: The array list object pointer
 *  value: The value to assign to the new item
 *  node: The node after which to perform the insertion
 */
QC_AL_NODE *qc_al_insert_after(QC_AL *restrict al, QC_VALUE value, QC_AL_NODE *restrict node);

/*
 * Inserts a node after another.
 *
 * Arguments:
 *  al: The array list object pointer
 *  value: The value to assign to the new item
 *  index: The index of the node after which to perform the insertion
 */
QC_AL_NODE *qc_al_insert_after_index(QC_AL *restrict al, QC_VALUE value, size_t index);

/*
 * Inserts a node before another.
 *
 * Arguments:
 *  al: The array list object pointer
 *  value: The value to assign to the new item
 *  node: The node before which to perform the insertion
 */
QC_AL_NODE *qc_al_insert_before(QC_AL *al, QC_VALUE value, QC_AL_NODE *node);

/*
 * Inserts a node before another.
 *
 * Arguments:
 *  al: The array list object pointer
 *  value: The value to assign to the new item
 *  index: The index of the node before which to perform the insertion
 */
QC_AL_NODE *qc_al_insert_before_index(QC_AL *restrict al, QC_VALUE value, size_t index);

/*
 * Appends an item at the end of the list.
 *
 *  al: The array list object pointer
 *  value: The value to assign to the new item
 */
#define qc_al_append(al, value) qc_al_insert_after_index((al), (value), (al)->length-1)

/*
 * Removes an item from the list.
 *
 * Arguments:
 *  al: The array list object pointer
 *  node: A pointer to the node to remove (after this function returns, node is freed and sheal not be accessed again)
 */
void qc_al_remove(QC_AL *restrict al, QC_AL_NODE *restrict node);

/*
 * Removes an item from the list.
 *
 * Arguments:
 *  al: The array list object pointer
 *  index: The index of the node to be removed
 */
void qc_al_remove_index(QC_AL *restrict al, size_t index);

#endif /* _QCOLLECT_ALIST_H */
