/*
 *    Qcollect: C collections library
 *    Copyright (C) 2015  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifndef NDEBUG
  #include <stdio.h>
#endif
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "alist.h"

/*
 * Resets the array list capacity.
 *
 * Arguments:
 *  list: Pointer to the array list object
 *  new_capacity: The new capacity to set to the list
 */
static void recap(QC_AL *list, size_t new_capacity) {
  list->arr = realloc(list->arr, sizeof(QC_AL_NODE*) * new_capacity);

  list->capacity = new_capacity;

  if(list->capacity < list->length) /* Shrunk */
    list->length = list->capacity;
}

/*
 * Displaces the elements with index >= offset of list by n places.
 *
 * Arguments:
 *  list: The array list pointer
 *  offset: The index of the first element to displace
 *  n: the number of places to displace the elements by
 *
 * Return value:
 *  0 if there has been no displacement, n if it has
 */
static int displace(QC_AL *list, size_t offset, int n) {
  if(offset >= list->capacity)
    return 0; /* Out of boundaries */

  if(n < 0 && ((size_t)abs(n) > list->length || (size_t)abs(n) > offset))
    return 0; /* New length would be negative or an element would be placed to index < 0 */

  size_t newlength = list->length + n;
  
  if(newlength > list->capacity)
    recap(list, newlength);

  {
    size_t i;

    if(n > 0) {
      i = list->length;
      while(i > offset) { /* Can't use a for loop 'cause size_t is unsigned */
        i--;
        list->arr[i+n] = list->arr[i];
      }
    } else {
      for(i = offset+n; i < offset; i++) /* Free elements that will be overridden */
        if(list->arr[i])
          free(list->arr[i]);

      for(i = offset; i < list->length; i++)
        list->arr[i+n] = list->arr[i]; /* NOTE: do not change i+n to i-n since n here is negative */
    }
  }

  list->length += n;
  return n;
}

/*
 * Find and returns the node index of the given node.
 *
 * Arguments:
 *  list: A pointer to the array list object
 *  node: A pointer to the node to search in the list
 *  out: A pointer to a memory location that will hold the result
 *
 * Return value:
 *  true if the node has been found and `out` has been updated,
 *  false if the node has not been found and `out` has not been updated
 */
static bool find_node(QC_AL *list, QC_AL_NODE *node, size_t *out) {
  size_t i;

  for(i = 0; i < list->length; i++)
    if(list->arr[i] == node) {
      *out = i;
      return true;
    }

  return false;
}

QC_AL* qc_al_init() {
  return qc_al_init2(QCOLLECT_AL_DEFAULT_CAPACITY);
}

QC_AL* qc_al_init2(size_t init_cap) {
  QC_AL* al = malloc(sizeof(QC_AL));

  al->arr = calloc(init_cap, sizeof(QC_AL_NODE*));
  al->capacity = init_cap;
  al->length = 0;

  return al;
}

void qc_al_free(QC_AL *al) {
  if(al->arr != NULL) {
    size_t i;

    for(i = 0; i < al->length; i++)
      free(al->arr[i]);
  }

  free(al->arr);
  free(al);
}

QC_AL_NODE *qc_al_insert_after(QC_AL *restrict al, QC_VALUE value, QC_AL_NODE *restrict node) {
  size_t pivot;

  if(find_node(al, node, &pivot))
    return qc_al_insert_after_index(al, value, pivot);
  else return NULL; /* Node not found */
}

QC_AL_NODE *qc_al_insert_after_index(QC_AL *restrict al, QC_VALUE value, size_t index) {
  int displacement;

  index += 1; /* Start working AFTER the given index */

  #define DISPLACE_OP displace(al, index, 1)
  if((displacement = DISPLACE_OP) == 0) {
    recap(al, al->capacity + QCOLLECT_AL_DEFAULT_CAPACITY_INCREASE);

    if((displacement = DISPLACE_OP) == 0) {
      #ifndef NDEBUG
      fprintf(stderr, "Sdang! Still no displacement in qc_al_insert_after_index\n");
      #endif
      abort(); /* Still no displacement, something REEEEALLY bad must be going on out there */
    }
  }
  #undef DISPLACE_OP

  al->arr[index] = malloc(sizeof(QC_AL_NODE));
  memcpy(&al->arr[index]->val, &value, sizeof(QC_VALUE));

  if(displacement == 0)
    al->length++;

  return al->arr[index];
}

QC_AL_NODE *qc_al_insert_before(QC_AL *al, QC_VALUE value, QC_AL_NODE *node) {
  size_t pivot;

  if(find_node(al, node, &pivot))
    return qc_al_insert_before_index(al, value, pivot);
  else return NULL; /* Node not found */
}

QC_AL_NODE *qc_al_insert_before_index(QC_AL *restrict al, QC_VALUE value, size_t index) {
  int displacement;

  #define DISPLACE_OP displace(al, index, 1)
  if((displacement = DISPLACE_OP) == 0) {
    recap(al, al->capacity + QCOLLECT_AL_DEFAULT_CAPACITY_INCREASE);

    if((displacement = DISPLACE_OP) == 0) {
      #ifndef NDEBUG
      fprintf(stderr, "Sdang! Still no displacement in qc_al_insert_after_index\n");
      #endif
      abort(); /* Still no displacement, something REEEEALLY bad must be going on out there */
    }
  }
  #undef DISPLACE_OP

  al->arr[index] = malloc(sizeof(QC_AL_NODE));
  memcpy(&al->arr[index]->val, &value, sizeof(QC_VALUE));

  if(displacement == 0)
    al->length++;

  return al->arr[index];
}

void qc_al_remove(QC_AL *restrict al, QC_AL_NODE *restrict node) {
  size_t idx;

  if(find_node(al, node, &idx))
    qc_al_remove_index(al, idx);
}

void qc_al_remove_index(QC_AL *restrict al, size_t index) {
  int displacement = displace(al, index+1, -1);

  if(displacement == 0)
    al->length--;
}
