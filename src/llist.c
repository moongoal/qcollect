/*
 *    Qcollect: C collections library
 *    Copyright (C) 2015  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include <stdlib.h>
#include <string.h>
#include "llist.h"

QC_LL *qc_ll_init() {
  QC_LL *ll = malloc(sizeof(QC_LL));

  ll->first = NULL;
  ll->last = NULL;

  return ll;
}

void qc_ll_free(QC_LL *ll) {
  QC_LL_NODE *node, *next;

  node = ll->first;

  while(node != NULL) {
    next = node->next;
    free(node);
    node = next;
  }

  free(ll);
}

QC_LL_NODE *qc_ll_insert_after(QC_LL *restrict ll, QC_VALUE value, QC_LL_NODE *restrict node) {
  QC_LL_NODE *next;

  if(ll->first == NULL && node == NULL) { /* First element */
    node = malloc(sizeof(QC_LL_NODE));
    node->prev = NULL;
    node->next = NULL;
    memcpy(&node->val, &value, sizeof(QC_VALUE));

    /* Update list */
    ll->first = node;
    ll->last = node;

    return node;
  } else { /* Not first element */
    next = node->next;
    node->next = malloc(sizeof(QC_LL_NODE));
    memcpy(&node->next->val, &value, sizeof(QC_VALUE));
    node->next->prev = node;
    node->next->next = next;

    if(next != NULL)
      next->prev = node->next;

    if(ll->last->next != NULL) /* Update list */
      ll->last = ll->last->next;

    return node->next;
  }
}

QC_LL_NODE *qc_ll_insert_before(QC_LL *restrict ll, QC_VALUE value, QC_LL_NODE *restrict node) {
  QC_LL_NODE *prev;

  if(ll->first == NULL && node == NULL) { /* First element */
    node = malloc(sizeof(QC_LL_NODE));
    node->prev = NULL;
    node->next = NULL;
    memcpy(&node->val, &value, sizeof(QC_VALUE));

    /* Update list */
    ll->first = node;
    ll->last = node;

    return node;
  } else { /* Not first element */
    prev = node->prev;
    node->prev = malloc(sizeof(QC_LL_NODE));
    memcpy(&node->prev->val, &value, sizeof(QC_VALUE));
    node->prev->next = node;
    node->prev->prev = prev;

    if(prev != NULL)
      prev->next = node->prev;

    if(ll->first->prev != NULL) /* Update list */
      ll->first = ll->first->prev;

    return node->prev;
  }
}

void qc_ll_remove(QC_LL *restrict ll, QC_LL_NODE *restrict node) {
  QC_LL_NODE *prev, *next;

  prev = node->prev;
  next = node->next;

  free(node);

  if(prev != NULL) prev->next = next;
  if(next != NULL) next->prev = prev;

  /* Update list */
  if(ll->first == node)
    ll->first = next;
  if(ll->last == node)
    ll->last = prev;
}
